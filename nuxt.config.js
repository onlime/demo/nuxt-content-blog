const siteTitle = process.env.SITE_TITLE || 'DemoBlog'
const titleHome = process.env.TITLE_HOME || 'demo.pipo.blog'

export default {
    // Target: https://go.nuxtjs.dev/config-target
    target: 'static',

    // https://nuxtjs.org/blog/moving-from-nuxtjs-dotenv-to-runtime-config
    publicRuntimeConfig: {
        algoliaAppId: process.env.ALGOLIA_APP_ID,
        algoliaSearchOnlyKey: process.env.ALGOLIA_SEARCH_ONLY_KEY,
        algoliaIndex: process.env.ALGOLIA_INDEX || 'articles',
        algoliaHitsPerPage: process.env.ALGOLIA_HITS_PER_PAGE || 5,
        algoliaQueryBufferTime: process.env.ALGOLIA_QUERY_BUFFER_TIME || 300,
    },

    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        titleTemplate: '%s | ' + siteTitle,
        title: titleHome,
        htmlAttrs: {
            lang: 'en',
        },
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: '' },
            { name: 'format-detection', content: 'telephone=no' },
        ],
        link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [
        '~/plugins/helpers',
        '~/plugins/dateformat',
        '~/plugins/vue-instantsearch',
        '~plugins/vue-mobile-detection',
    ],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [
        // https://go.nuxtjs.dev/eslint
        '@nuxtjs/eslint-module',
        // https://go.nuxtjs.dev/tailwindcss
        '@nuxtjs/tailwindcss',
        // https://color-mode.nuxtjs.org/
        '@nuxtjs/color-mode',
        // https://google-fonts.nuxtjs.org
        // https://www.elian.codes/blog/adding-google-fonts-to-your-nuxtjs-site/
        '@nuxtjs/google-fonts',
        // https://github.com/nuxt-community/fontawesome-module
        '@nuxtjs/fontawesome',
        // https://image.nuxtjs.org
        '@nuxt/image',
        // https://www.npmjs.com/package/nuxt-content-algolia
        'nuxt-content-algolia',
    ],

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
        // https://go.nuxtjs.dev/content
        '@nuxt/content',
    ],

    // Content module configuration: https://go.nuxtjs.dev/config-content
    content: {},

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {
        transpile: ['vue-instantsearch', 'instantsearch.js/es'],
    },

    hooks: {
        // https://content.nuxtjs.org/advanced#contentfilebeforeinsert
        'content:file:beforeInsert': (document) => {
            // https://github.com/ngryman/reading-time
            if (document.readingTimeMinutes) {
                document.readingTime = document.readingTimeMinutes + ' min read'
            } else if (document.extension === '.md') {
                const { text } = require('reading-time')(document.text)
                document.readingTime = text
            }

            // https://www.npmjs.com/package/remove-markdown
            if (document.extension === '.md') {
                const removeMd = require('remove-markdown')
                document.bodyPlainText = removeMd(document.text)
            }
        },
    },

    // https://color-mode.nuxtjs.org/
    colorMode: {
        // removing the default '-mode' suffix as Tailwind only expects class name `dark`
        classSuffix: '',
    },

    // https://google-fonts.nuxtjs.org
    // https://themeisle.com/blog/best-google-fonts/
    googleFonts: {
        families: {
            // 'Open+Sans': true,
            Roboto: true,
            Quicksand: true, // logo font, also used by NuxtJS docs
        },
    },

    fontawesome: {
        icons: {
            // solid: true,
            // brands: true,
            solid: ['faTimes', 'faHamburger'],
            brands: [],
        },
    },

    // https://www.npmjs.com/package/nuxt-content-algolia
    nuxtContentAlgolia: {
        appId: process.env.ALGOLIA_APP_ID,
        // !IMPORTANT secret key should always be an environment variable
        // this is not your search only key but the key that grants access to modify the index
        apiKey: process.env.ALGOLIA_API_KEY,
        // relative to content directory - each path get's its own index
        paths: [
            {
                name: 'articles',
                index: process.env.ALGOLIA_INDEX || 'articles',
                fields: ['title', 'description', 'bodyPlainText', 'tags'],
            },
        ],
    },
}
