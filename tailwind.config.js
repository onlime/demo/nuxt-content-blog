/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
    mode: 'jit',
    purge: [
        './components/**/*.{js,jsx,ts,tsx,vue}',
        './layouts/**/*.{js,jsx,ts,tsx,vue}',
        './pages/**/*.{js,jsx,ts,tsx,vue}',
    ],
    darkMode: 'class',
    plugins: [require('@tailwindcss/typography')],
    theme: {
        fontFamily: {
            // default Roboto https://fonts.google.com/specimen/Roboto
            sans: 'Roboto, ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"',
        },
        extend: {
            opacity: {
                96: '.96',
                97: '.97',
                98: '.98',
            },
            colors: {
                gray: {
                    150: '#ECECEE',
                    350: '#D1D5DB',
                    950: '#090C14',
                },
            },
        },
    },
}
