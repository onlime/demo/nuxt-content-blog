// vue-mobile-detection
// https://www.npmjs.com/package/vue-mobile-detection
import Vue from 'vue'
import VueMobileDetection from 'vue-mobile-detection'

Vue.use(VueMobileDetection)
/* You can use this.$isMobile() in all your vue components */
