import { format, parseISO } from 'date-fns'

export default (_context, inject) => {
    inject('formatDate', (dateString) => format(parseISO(dateString), 'PPP'))
}
