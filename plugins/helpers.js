// Debounce function similar to Lodash _.debounce()
// https://lodash.com/docs/current#debounce
const debounceStack = []
const debounce = (delay, key = 'global') => {
    return new Promise((resolve) => {
        clearTimeout(debounceStack[key])
        debounceStack[key] = setTimeout(resolve, delay)
    })
}

export default (_context, inject) => {
    inject('debounce', debounce)
}
